import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function load(component) {
  return () => System.import(`components/${component}.vue`)
}

export default new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */

  routes: [
    {
      path: '/',
      component: load('main'),
      children: [
        { path: '/1', component: load('window1') }, // Default
        { path: '/2', component: load('window2') }, // Default
        { path: '/3', component: load('window3') }, // Default
        { path: '/4', component: load('window4') }, // Default
        { path: '/5', component: load('window5') }, // Default
        { path: '/tablo', component: load('tablo') }, // Default
      ]
    }, // Default
    { path: '*', component: load('Error404') } // Not found
  ]
})
