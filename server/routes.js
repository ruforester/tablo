const Pages = require('./handlers/pages')

module.exports = [{
    method: 'GET',
    path: '/{param*}',
    handler: Pages.servePublicDirectory
}]