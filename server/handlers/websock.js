

exports.onMessage = function (socket, message, next) {
    const server = socket.server
    // console.log(`Message from socket ${socket.id}: ${message}`)

    server.app.windows.forEach((element) => {
        if (element.number === message.number) {
            element.state = message.state
        }
    })
    server.broadcast(server.app.windows)
    return next
}

exports.onConnection = function (socket) {
    const server = socket.server 
    console.log(`Socket ${socket.id} connected`)
    server.broadcast(server.app.windows)
}

exports.onDisconnection = function (socket) {
    console.log(`Socket ${socket.id} disconnected`)
}

