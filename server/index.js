const Hapi = require('hapi')
const Websock = require('./handlers/websock')

const server = new Hapi.Server()
server.connection({
  port: 3000
})

server.register([
  {
    register: require('inert')
  },
  {
    register: require('nes'),
    options: {
      onConnection: Websock.onConnection,
      onDisconnection: Websock.onDisconnection,
      onMessage: Websock.onMessage
    }
  }
], (err) => {

  if (err) {
    throw err
  }

  server.route(require('./routes'))

  server.app.windows = [
    {
      number: 1,
      state: 'nowork'
    },
    {
      number: 2,
      state: 'nowork'
    },
    {
      number: 3,
      state: 'nowork'
    },
    {
      number: 4,
      state: 'nowork'
    },
    {
      number: 5,
      state: 'nowork'
    }
  ]

  server.start((err) => {
    if (err) {
      throw err
    }
    console.log(`Server listening on ${server.info.uri}`)
    let windows = [
      {
        number: 1,
        state: 'nowork'
      },
      {
        number: 2,
        state: 'nowork'
      },
      {
        number: 3,
        state: 'nowork'
      },
      {
        number: 4,
        state: 'nowork'
      },
      {
        number: 5,
        state: 'nowork'
      }
    ]

    server.broadcast(windows)
  })
})